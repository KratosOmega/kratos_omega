Crack the Coding Interview 189
Personal CCI 189 Answers

8.14 Boolean Evaluation:
Given a boolean expression consisting of the symbols "0" (false), "1" (true), "&" (AND), "|" (OR), and "^" (XOR), and a desired boolean result value "result",
implement a function to count the number of ways of parenthesizing the expression such that it evaluates to result.

EXAMPLE
countEval("1^0|0|1", false) -> 2
countEval("0&0&0&1^1|0", true) -> 10
