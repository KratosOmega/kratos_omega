package com.chapter01._9_String_Rotation;

/**
 * Created by Kratos on 4/21/16.
 */

public class Solution {
    public boolean isRotation(String s1, String s2) {
        //string.length(); will be easier for this problem
        char[] c1 = removeWhiteSpaces(s1).toCharArray();
        char[] c2 = removeWhiteSpaces(s2).toCharArray();
        String s1s1 = toString(removeWhiteSpaces(s1 + s1).toCharArray());

        //First filter test: different length => no substring
        if (c1.length == c2.length && c1.length != 0) {
            //Call 'isSubstring' ONLY ONCE!
            return isSubstring(s1s1, toString(c2));
        }
        System.out.println("False");
        return false;
    }

    //Assume the isSubstring method (it actually means we can use .indexOf() method) is avaiable
    private boolean isSubstring(String big, String small) {
        if (big.indexOf(small) >= 0) {
            System.out.println("True");
            return true;
        } else {
            System.out.println("False");
            return false;
        }
    }

    //removeWhiteSpaces() removes all white spaces for better analysis environment
    private String removeWhiteSpaces(String s) {
        String string = "";
        char[] charList = s.toCharArray();

        for (char c : charList) {
            if (c == ' ') {
                continue;
            } else {
                string += c;
            }
        }
        return string;
    }

    //toString() converts char Array to String
    private String toString(char[] charList) {
        String string = "";
        for (char c : charList) {
            string += c;
        }
        return string;
    }
}
