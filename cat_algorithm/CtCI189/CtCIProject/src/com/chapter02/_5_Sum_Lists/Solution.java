package com.chapter02._5_Sum_Lists;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/24/16.
 *
 * <code>backwardSumList</code>: use iteration method
 * <code>forwardSumList</code>: use recursive method
 *
 * NOTE:
 * <code>forwardSumList</code> need a CONSTANT to store the original length of the linked list
 * CONSTANT is used later to remove the '0' after the beginning of the linked list
 *
 */

public class Solution {
    public LinkedListNode backwardSumList(library.linkedList.LinkedListNode a, LinkedListNode b) {
        //variables for fixing the equal length
        LinkedListNode aClone = a;
        LinkedListNode bClone = b;
        int aLength = a.size();
        int bLength = b.size();
        int length = 0;

        //variables for addition of each pair of nodes
        LinkedListNode sum = new LinkedListNode();
        //~ sum: it will always refer to the head of the sum linked list
        //~ using tempSum to update each node of sum linked list
        LinkedListNode tempSum = sum;
        //~ tempSum: used for going through and update each sum node
        //~ when calculation is done, tempSum is at the end node of sum
        int addition;

        //make length equal, fill the short part with '0'
        //O(n)
        if (aLength != bLength) {
            if (aLength > bLength) {
                int diff = aLength - bLength;
                while (bClone.getNext() != null) {
                    bClone = bClone.getNext();
                    if (bClone.getNext() == null && diff > 0) {
                        bClone.setNext(new LinkedListNode());
                        diff--;
                    }
                }
                length = aLength;
            } else {
                int diff = bLength - aLength;
                while (aClone.getNext() != null) {
                    aClone = aClone.getNext();
                    if (aClone.getNext() == null && diff > 0) {
                        aClone.setNext(new LinkedListNode());
                        diff--;
                    }
                }
                length = bLength;
            }
        } else {
            length = aLength;
        }

        //addition of nodes
        //O(n)
        while (length > 0) {
            addition = tempSum.getData() + a.getData() + b.getData();
            if (addition >= 10) {
                addition = addition - 10;
                tempSum.setData(addition);
                tempSum.setNext(new LinkedListNode(1));
                tempSum = tempSum.getNext();
                a = a.getNext();
                b = b.getNext();
                length--;
            } else {
                if (length == 1) {
                    tempSum.setData(addition);
                    length--;
                } else {
                    tempSum.setData(addition);
                    tempSum.setNext(new LinkedListNode(0));
                    tempSum = tempSum.getNext();
                    a = a.getNext();
                    b = b.getNext();
                    length--;
                }
            }
        }
        return sum;
    }

    public LinkedListNode forwardSumList(LinkedListNode a, LinkedListNode b) {
        //variables for addition of each pair of nodes
        int tempSum;
        //variable for fixing equal length
        int aLength = a.size();
        int bLength = b.size();
        LinkedListNode aClone = a;
        LinkedListNode bClone = b;
        final int LENGTH;

        //add 0s to make 2 number list to be equal length
        if (aLength != bLength) {
            if (aLength > bLength) {
                int m = 0;
                while (m < aLength - bLength) {
                    LinkedListNode newNode = new LinkedListNode(bClone.getData());
                    newNode.setNext(bClone.getNext());

                    bClone.setNext(newNode);
                    bClone.setData(0);
                    m++;
                }
                LENGTH = aLength;
            } else {
                int n = 0;
                while (n < bLength - aLength) {
                    LinkedListNode newNode = new LinkedListNode(aClone.getData());
                    newNode.setNext(aClone.getNext());
                    aClone.setNext(newNode);
                    aClone.setData(0);
                    n++;
                }
                LENGTH = bLength;
            }
        } else {
            LENGTH = aLength;
        }

        if (a.getNext() == null) {
            tempSum = a.getData() + b.getData();
            if (tempSum >= 10) {
                tempSum = tempSum - 10;
                a.setNext(new LinkedListNode(tempSum));
                //or I can use 'a.appendAtTail(tempSum);'
                a.setData(1);
                return a;
            } else {
                a.setNext(new LinkedListNode(tempSum));
                a.setData(0);
                return a;
            }
        } else {
            tempSum = forwardSumList(a.getNext(), b.getNext()).getData() + a.getData() + b.getData();
            if (tempSum >= 10) {
                tempSum = tempSum - 10;
                a.getNext().setData(tempSum);
                //or I can use 'a.appendAtTail(tempSum);'
                a.setData(1);
                return a;
            } else {
                a.getNext().setData(tempSum);
                a.setData(0);
                if (a.size() > LENGTH) {
                    a.setData(a.getNext().getData());
                    a.setNext(a.getNext().getNext());
                }
                return a;
            }
        }
    }
}
