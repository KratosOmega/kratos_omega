package Q1_02_Check_Permutation;

public class QuestionB {
	public static boolean permutation(String s, String t) {
		if (s.length() != t.length()) {
			return false;
		}

		int[] letters = new int[128];

    //KEYPOINT: 1st string, for each character, mark their existence by add 1 to the letters array
    // The letters array length is int[128] which is the character total range
		char[] s_array = s.toCharArray();
		for (char c : s_array) { // count number of each char in s.
			letters[c]++;
		}

    //KEYPOINT: 2nd string, for each character, mark their existence by minus 1 to the letters array
    // Thus, if these 2 string are same, the letters array will be all "0" in every cell. 
		for (int i = 0; i < t.length(); i++) {
			int c = (int) t.charAt(i);
			letters[c]--;
		    if (letters[c] < 0) {
		    	return false;
		    }
		}

		return true;
	}

	public static void main(String[] args) {
		String[][] pairs = {{"apple", "papel"}, {"carrot", "tarroc"}, {"hello", "llloh"}};
		for (String[] pair : pairs) {
			String word1 = pair[0];
			String word2 = pair[1];
			boolean anagram = permutation(word1, word2);
			System.out.println(word1 + ", " + word2 + ": " + anagram);
		}
	}
}
