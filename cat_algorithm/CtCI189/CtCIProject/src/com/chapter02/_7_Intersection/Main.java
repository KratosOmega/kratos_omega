package com.chapter02._7_Intersection;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/24/16.
 */
public class Main {
    public static void main(String[] args) {
        LinkedListNode a = new LinkedListNode(1);
        LinkedListNode b = new LinkedListNode(1);
        LinkedListNode jointNode = new LinkedListNode(101);
//        LinkedListNode b = jointNode;
        Solution solution = new Solution();

        //**************** Set up Linked Lists' data
        a.appendAtTail(2);
        a.appendAtTail(3);
        a.appendAtTail(4);
        a.appendAtTail(5);
        a.appendAtTail(6);
        // ~~~~~~~~~~~~~~~~~~~~~~~
        b.appendAtTail(2);
        b.appendAtTail(3);
        // ~~~~~~~~~~~~~~~~~~~~~~~
//        jointNode.appendAtTail(1);
//        jointNode.appendAtTail(2);
//        jointNode.appendAtTail(3);

        //************************************* Display Linked Lists before Joint
        System.out.printf("# 1 Linked List before Joint: ");
        a.print();
        System.out.println("");
        System.out.printf("# 2 Linked List before Joint: ");
        b.print();
        System.out.println("");
        System.out.println("");

        //************************ Making Joint Node
        connectNodes(a, jointNode);
        connectNodes(b, jointNode);

        //************************************* Display Linked Lists before Joint
        System.out.printf("# 1 Linked List after Joint: ");
        a.print();
        System.out.println("");
        System.out.printf("# 2 Linked List after Joint: ");
        b.print();
        System.out.println("");
        System.out.println("");;

        System.out.println("--->>>>> Test <<<<<---");
        if(solution.intersection(a, b)!= null){
            System.out.printf("Joint node is: ");
            System.out.println(solution.intersection(a, b).getData());
        }else{
            System.out.println("No Joint node is founded");
        }
    }

    public static void connectNodes(LinkedListNode begin, LinkedListNode end){
        while (begin.getNext() != null){
            begin = begin.getNext();
        }
        begin.setNext(end);
    }
}
