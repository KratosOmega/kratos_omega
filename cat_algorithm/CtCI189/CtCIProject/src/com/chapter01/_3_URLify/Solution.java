package com.chapter01._3_URLify;

/**
 * Created by Kratos on 4/19/16.
 *
 * The original problem will give both inputs: "string" & "length of string"
 * Solution_A below will ONLY use the "string" as input
 */

public class Solution {
    public String urlify(String str) {
        int num = 0;
        String newString = "";
        char[] strChar = str.toCharArray();
        int[] charIndex = new int[charCount(strChar)];

        //Find non-white-spaces characters' index
        for (int i = 0; i < strChar.length; i++) {
            if (strChar[i] != ' ') {
                charIndex[num] = i;
                num++;
            }
        }

        //Add non-white-spaces characters to the return string
        //Add validation to add "%20"
        for (int i = 0; i < charIndex.length; i++) {
            if (i == 0) {
                newString += strChar[charIndex[i]];
            } else {
                if (charIndex[i] - charIndex[i - 1] == 1) {
                    newString += strChar[charIndex[i]];
                } else {
                    newString += "%20";
                    newString += strChar[charIndex[i]];
                }
            }
        }
        return newString;
    }

    //Private method for determine non-white-spaces characters' count
    private int charCount(char[] stringInChar) {
        int count = 0;

        for (int i = 0; i < stringInChar.length; i++) {
            if (stringInChar[i] != ' ') {
                count += 1;
            }
        }
        return count;
    }
}
