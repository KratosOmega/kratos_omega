package com._151;

import java.util.ArrayList;

/**
 * Created by Kratos on 4/18/16.
 * <p/>
 * 151. Reverse Words in a String
 * https://leetcode.com/problems/reverse-words-in-a-string/
 * <p/>
 * Given an input string, reverse the string word by word.
 * For example,
 * Given s = "the sky is blue",
 * return "blue is sky the".
 * <p/>
 * <p/>
 * Update (2015-02-12):
 * For C programmers: Try to solve it in-place in O(1) space.
 */

public class Solution {
    public String reverseWords(String s) {
        s = s.trim();
        // .trim() is the 1st key point in order to solve this problem
        // .trim() helps get rid of the whitespaces before and after the string
        // if the whitespaces exist when during later loop, there will be errors
        String[] origOrder = s.split("\\s+");
        // \\s+ is used for 1 or more whitespaces, tabs, and lines patterns detection
        ArrayList<String> revertedOrder = new ArrayList<String>();
        String finalString = "";

        for (int i = origOrder.length - 1; i >= 0; i--) {
            revertedOrder.add(origOrder[i]);
        }

        for (int i = 0; i < revertedOrder.size(); i++) {

            if (i == revertedOrder.size() - 1) {
                finalString = finalString + revertedOrder.get(i);
            } else {
                finalString = finalString + revertedOrder.get(i) + " ";
            }
        }
        return finalString;
    }
}
