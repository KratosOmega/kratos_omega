package com.chapter02._1_Remove_Dups;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/22/16.
 */
public class Main {
    public static void main(String[] args) {
        LinkedListNode head = new LinkedListNode(0);

        head.appendAtTail(1);
//        head.appendAtTail(1);
//        head.appendAtTail(2);
//        head.appendAtTail(2);
//        head.appendAtTail(2);
//        head.appendAtTail(3);
//        head.appendAtTail(4);
//        head.appendAtTail(5);
//        head.appendAtTail(5);
//        head.appendAtTail(4);
//        head.appendAtTail(6);
//        head.appendAtTail(1);
//        head.appendAtTail(2);
//        head.appendAtTail(2);

        head.print();
        System.out.println("");

        Solution solution = new Solution();
        solution.removeDuplicatesWithoutTempporaryBuffer(head);

        head.print();
    }
}
