package com.chapter02._3_Delete_Middle_Node;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/23/16.
 */

public class Solution {
    public void deleteMiddleNode(LinkedListNode node){
        //Replace the data data of target node with next node's data data
        //Thus, target node and its next bode are same now
        node.setData(node.getNext().getData());
        //Remove target node's next node
        node.setNext(node.getNext().getNext());

        //Actually, the target node for deletion is never deleted
        //Since we only have access start from the target node
        //There is no way to remove it from the linked list
        //We can ONLY remove nodes after the target node
    }
}
