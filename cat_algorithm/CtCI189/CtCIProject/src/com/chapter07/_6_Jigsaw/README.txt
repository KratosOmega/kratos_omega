Crack the Coding Interview 189
Personal CCI 189 Answers

7.6 Jigsaw:
Implement an NxN jigsaw puzzle. Design the data structures and structures and explain an algorithm to solve the puzzle.
You can assume that you have a "fitsWith" method which, when passed two puzzle edges, returns true if the two edges belong together.
