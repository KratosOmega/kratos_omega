Crack the Coding Interview 189
Personal CCI 189 Answers

10.3 Search in Rotated Array:
Given a sorted array of "n" integers that has been rotated an unknown number of times, write code to find an element in the array.
You may assume that the array was originally sorted in increasing order.

EXAMPLE
Input:      find 5 in {15, 16, 19, 20, 25, 1, 3, 5, 6, 7, 10, 14}
Output:     8 (the index of 5 in the array)
