package com.chapter02._8_Loop_Detection;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/24/16.
 */
public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        LinkedListNode nonLoop = new LinkedListNode();
        LinkedListNode loop = new LinkedListNode();
        LinkedListNode joint = loop;

        //********************** Set up values
        nonLoop.appendAtTail(1);
        nonLoop.appendAtTail(2);
        nonLoop.appendAtTail(3);
        nonLoop.appendAtTail(4);
        nonLoop.appendAtTail(5);

        loop.appendAtTail(1);
        loop.appendAtTail(2);
        loop.appendAtTail(3);
        loop.appendAtTail(4);
        loop.appendAtTail(5);

        //****************************************** Display Linked List
        System.out.println("non-loop Linked List:");
        nonLoop.print();
        System.out.println("");
        System.out.println("");

        System.out.printf("Joint Point is set at: ");
        System.out.println(joint.getNext().getNext().getData());
        System.out.println("");

        System.out.println("loop Linked List:        ");
        loop.print();
        System.out.println(" -> " + joint.getNext().getNext().getData());

        //***************************************************************************************** Set [5] (.getNext() x 5) -> [2] (.getNext() x 2)
        //      [1]        [2]      [3]       [4]      *[5]*      ->            [1]       *[2]*
        joint.getNext().getNext().getNext().getNext().getNext().setNext(joint.getNext().getNext());
//        //******************* Find the FIRST loop point
//        //      [0] -> [0]
//        joint.setNext(joint);

        //********************* Test Run
        System.out.println("");
        System.out.println("Detection Test: ");
        System.out.println("----->>>>> Non-Loop Linked List Test <<<<<-----");
        if (solution.loopDetection(nonLoop) == null) {
            System.out.println("No loop is detected in this linked list");
        }

        System.out.println("");
        System.out.println("----->>>>> Loop Linked List Test <<<<<-----");
        System.out.println(solution.loopDetection(loop).getData());
    }
}
