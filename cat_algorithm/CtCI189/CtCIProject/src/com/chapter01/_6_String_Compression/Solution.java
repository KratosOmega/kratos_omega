package com.chapter01._6_String_Compression;
/**
 * Created by Kratos on 4/20/16.
 *
 * This class works only for string that contains ONLY a-z & A-Z
 */

public class Solution {
    public String compression(String s){
        char[] letterList = s.toCharArray();
        char[] comp = new char[2];
        comp[0] = ' ';
        comp[1] = ' ';
        String compressedString = "";

        for(char c : letterList){
            if (comp[0] == ' '){
                comp[0] = c;
                comp[1] = '1';
            }else{
                if(comp[0] == c){
                    comp[1] ++;
                }
                else{
                    compressedString += comp[0];
                    compressedString += comp[1];
                    comp[0] = c;
                    comp[1] = '1';
                }
            }
        }
        compressedString += comp[0];
        compressedString += comp[1];
        return compressedString;
    }
}
