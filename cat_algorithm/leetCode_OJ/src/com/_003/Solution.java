package com._003;

import java.util.ArrayList;

/**
 * Created by Kratos on 4/18/16.
 * <p/>
 * 3. Longest Substring Without Repeating Characters
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/
 * <p/>
 * Given a string, find the length of the longest substring without repeating characters.
 * <p/>
 * Examples:
 * Given "abcabcbb", the answer is "abc", which the length is 3.
 * Given "bbbbb", the answer is "b", with the length of 1.
 * Given "pwwkew", the answer is "wke", with the length of 3.
 * <p/>
 * Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */

public class Solution {
    public int lengthOfLongestSubstring(String s) {
        // Fields
        char[] list = s.toCharArray();
        ArrayList<Character> tempList;
        int startPonint = 0;
        int nextOne = startPonint + 1;
        int max = 1;
        int updateMax = 0;
        boolean plusPlus = true;


        if (list.length == 0) {
            return 0;
        }
        if (list.length == 1) {
            return 1;
        }

        while (nextOne < list.length) {
            // Loop restarts here
            plusPlus = true;
            max = 1;
            // Restart counting the max length
            for (int x = startPonint; x < nextOne; x++) {
                if (list[x] != list[nextOne]) {
                    max++;
                    // As long as no duplication, max++;
                } else {
                    startPonint = x + 1;
                    nextOne = startPonint + 1;
                    plusPlus = false;
                    updateMax = updateMax > max ? updateMax : max;
                    break;
                    // Founded duplication:
                    // Reset startPoint to the duplicated index's next
                    // Reset the nextOne to be startPoint+1
                    // Update the final max;
                }
            }
            if (plusPlus) {
                nextOne++;
            }
            updateMax = updateMax > max ? updateMax : max;
        }
        return updateMax;
    }
}
