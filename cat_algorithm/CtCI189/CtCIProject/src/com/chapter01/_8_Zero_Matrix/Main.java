package com.chapter01._8_Zero_Matrix;

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[][] matrix = {{0,2,3,4,5},
                          {1,2,3,4,5},
                          {5,4,3,0,1},
                          {5,4,3,2,1},
                          {6,7,8,9,0}};

        solution.zeroMatrix(matrix);
    }
}
