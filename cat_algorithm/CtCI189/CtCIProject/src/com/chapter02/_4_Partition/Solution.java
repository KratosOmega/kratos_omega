package com.chapter02._4_Partition;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/23/16.
 */

public class Solution {
    public void nodePartition(LinkedListNode node, int partition){
        int count = 1;
        int length = node.size();
        LinkedListNode end = node;

        while (end.getNext() != null){
            end = end.getNext();
        }

        while (count < length){
            if (node.getData() >= partition){
                //make a new node (which is a copy of 'node')
                //attach new node to the 'end'
                //update 'end' to the end of the linked list
                end.setNext(new LinkedListNode(node.getData()));
                end = end.getNext();
                //remove 'node', by clone next node to 'node'
                //then detach with next node
                //then connect cloned 'node' with next, next node
                node.setData(node.getNext().getData());
                node.setNext(node.getNext().getNext());
                count++;
            }else{
                node = node.getNext();
                count++;
            }
        }
    }
}
