package com.chapter02._8_Loop_Detection;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/24/16.
 * <p/>
 * Time complexity = O(n*n) ?
 */

public class Solution {
    public LinkedListNode loopDetection(LinkedListNode node) {
        LinkedListNode comparList = node;
        final LinkedListNode HEAD = node;   //Constant <code>HEAD</code> always point to the beginning of the linked list

        while (node.getNext() != null) {
            node = node.getNext();  //<code>node</code> here is used to determine if it is the corruptted node
            comparList.setNext(null); //Cut off the linked list
            comparList = HEAD;    //Reset the position of <code>comparList</code> to the beginning of the linked list

            //IMPORTANT: in the code below
            //it is important to have if(b) after if(a)
            //it allows if(a) to run the final comparison for last node in <code>comparList</code>
            //and if(b) will break the while loop before it set <code>comparList</code> current position in the last node to NULL
            //thus, <code>comparList</code> can still use this last node to connect with <code>node</code> in order to continue the outter loop
            while (true) {
                if (node == comparList) { //if(a)
                    return node;
                }
                if (comparList.getNext() == null) {   //if(b)
                    break;
                }
                comparList = comparList.getNext();
            }
            comparList.setNext(node);   //Reconnect 2 linked list
            comparList = comparList.getNext();  //Update <code>comparList</code> current position to the next
        }
        return null;
    }
}


////----------------- Old Method (v1) ------------------------
//public class Solution_A {
//    public LinkedListNode loopDetection(LinkedListNode node) {
//        LinkedListNode comparList = node;
//        final LinkedListNode HEAD = node;
//
//        while (node.getNext() != null) {
//            node = node.getNext();
//            comparList.setNext(null);
//            comparList = HEAD;
//            if (comparList.getNext() != null) {
//                while (comparList.getNext() != null) {
//                    if (node == comparList) {
//                        return node;
//                    }
//                    comparList = comparList.getNext();
//                    if (comparList.getNext() == null) {
//                        if (node == comparList) {
//                            return node;
//                        }
//                    }
//                }
//            } else {
//                if (node == comparList) {
//                    return node;
//                }
//            }
//            comparList.setNext(node);
//            comparList = comparList.getNext();
//        }
//        return null;
//    }
//}
