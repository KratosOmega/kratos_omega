package com.chapter01._8_Zero_Matrix;

/**
 * Created by Kratos on 4/20/16.
 */

public class OnlineSolution {
    // use boolean array
    static void setZeros(int[][] matrix) {
        boolean[] rows = new boolean[matrix.length];
        boolean[] cols = new boolean[matrix[0].length];

        // mark zero
        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[0].length; ++j) {
                if (matrix[i][j] == 0) {
                    rows[i] = cols[j] = true;
                }
            }
        }

        // set zeros
        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[0].length; ++j) {
                if (rows[i] || cols[j]) {
                    matrix[i][j] = 0;
                }
            }
        }
    }

    // use bit vector
    static void setZeros2(int[][] matrix) {
        long bitVecRows = 0;
        long bitVecCols = 0;

        // mark zero
        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[0].length; ++j) {
                if (matrix[i][j] == 0) {
                    bitVecRows |= 1l << i;
                    bitVecCols |= 1l << j;
                }
            }
        }

        // set zeros
        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[0].length; ++j) {
                if ((bitVecRows & (1l << i)) != 0 || (bitVecCols & (1l << j)) != 0) {
                    matrix[i][j] = 0;
                }
            }
        }
    }

    //TEST----------------------------------
    public static void main(String[] args) {
        int[][] a = {{0,2,3,4,5},
                {1,2,3,4,5},
                {5,4,3,0,1},
                {5,4,3,2,1},
                {6,7,8,9,0}};
        setZeros(a);
        printArray(a);
    }

    private static void printArray(int[][] a) {
        for (int[] row : a) {
            for (int col : row) {
                System.out.printf(col + " ");
            }
            System.out.println();
        }
    }
}
