package com.chapter01._5_One_Away;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();

        //Pass Zone
        System.out.println("-------- Pass Zone --------");
        if(solution.oneOrNoEdit("12345","12345") == true){
           System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit("12345","1235") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit("12345","123456") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit("12 345","12345") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit("123 45","123 45") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit("1234 5","1234 56") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit("134 5","1234 5") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit(" 12345","12345") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit(" 12345"," 122345") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit("1 234 5","1 24 5") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit("12945","12345") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit("122345","1222345") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit(" 12345"," 2345") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }


        //Fail Zone
        System.out.println("-------- Fail Zone --------");
        if(solution.oneOrNoEdit(" 12345","   12345") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit(" 12345","1 2345") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit("1123459","12345") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }

        if(solution.oneOrNoEdit("12 45","123 5") == true){
            System.out.println("PASS");
        }else{
            System.out.println("X");
        }
    }
}
