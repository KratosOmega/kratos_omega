package com.chapter01._6_String_Compression;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.compression("aabcccccaaa"));
        System.out.println(solution.compression("aaaaabbbbcccdde"));
    }
}
