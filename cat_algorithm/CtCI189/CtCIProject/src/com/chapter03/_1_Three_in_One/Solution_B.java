package com.chapter03._1_Three_in_One;

import java.util.Arrays;

/**
 * Created by Kratos on 4/26/16.
 * <p/>
 * <p/>
 * B)   Using linked list concept
 * By using the linked list concept, we can locate the previous block for the same stack in a single array
 * Thus, it makes dramatically allocation of stack spaces to different stack possible
 */

public class Solution_B {
    private final int STACK_SIZE;
    private final int STACK_NUM;
    private int[] stackPointers;
    private StackNode[] buffer;
    int indexUsed = 0;

    public Solution_B(int STACK_SIZE, int STACK_NUM) {
        this.STACK_SIZE = STACK_SIZE;
        this.STACK_NUM = STACK_NUM;
        Arrays.fill(this.stackPointers, -1);
        buffer = new StackNode[this.STACK_SIZE * this.STACK_NUM];
    }

    void push(int stackNum, int value) {
        int lastIndex = stackPointers[stackNum];
        stackPointers[stackNum] = indexUsed;
        indexUsed++;
        buffer[stackPointers[stackNum]] = new StackNode(lastIndex, value);
    }

    int pop(int stackNum) {
        int value = buffer[stackPointers[stackNum]].data;
        int lastIndex = stackPointers[stackNum];
        stackPointers[stackNum] = buffer[stackPointers[stackNum]].previous;
        buffer[lastIndex] = null;
        indexUsed--;
        return value;
    }

    int peek(int stack) {
        return buffer[stackPointers[stack]].data;
    }

    boolean isEmpty(int stackNum) {
        return stackPointers[stackNum] == -1;
    }

    class StackNode {
        public int previous;
        public int data;

        public StackNode(int previous, int data) {
            this.data = previous;
            this.previous = data;
        }
    }
}
