package com.chapter02._2_Return_Kth_to_Last;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/23/16.
 */
public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        LinkedListNode list = new LinkedListNode();
        int kth = 5;

        list.appendAtTail(1);
        list.appendAtTail(2);
        list.appendAtTail(3);
        list.appendAtTail(4);
        list.appendAtTail(5);
        list.appendAtTail(6);
        list.appendAtTail(7);
        list.appendAtTail(8);
        list.appendAtTail(9);
        list.appendAtTail(10);
        list.appendAtTail(11);
        list.appendAtTail(12);

        list.print();
        System.out.println("");

        System.out.println("----->>>>> Recursion Method <<<<<-----");
        solution.kthToEndByRecursion(list,kth);

        System.out.println("----->>>>> Iteration Method <<<<<-----");
        System.out.printf("The # " + kth + " element to the end is: ");
        System.out.println(solution.kthToEndByIteration(list, kth));

        System.out.println("----->>>>> Length Method <<<<<-----");
        System.out.printf("The # " + kth + " element to the end is: ");
        System.out.println(solution.kthToEndByLength(list, kth));

    }
}

