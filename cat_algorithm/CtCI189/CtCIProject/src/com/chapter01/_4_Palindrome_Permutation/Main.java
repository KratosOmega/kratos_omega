package com.chapter01._4_Palindrome_Permutation;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.isPermutationAndPalindrome("civic");
        solution.isPermutationAndPalindrome("hellolleh");
        solution.isPermutationAndPalindrome("asdfoio");
        solution.isPermutationAndPalindrome("ci    v ic    ");
        solution.isPermutationAndPalindrome("qwertyuiop qpwoeiruty xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        solution.isPermutationAndPalindrome("asdfoiwjoiwnfasnvoadfvjoaisdjfoaisjdoifasodjfoaisjdfoajsdoifjaosidjfoiasjdof");
    }
}
