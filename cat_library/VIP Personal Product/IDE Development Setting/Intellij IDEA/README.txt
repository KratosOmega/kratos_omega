
To export IDE settings to a JAR archive

On the main menu, choose File | Export Settings.
In the Export Settings dialog box that opens specify the settings to export by selecting the check boxes next to them. By default, all settings are selected.
In the Export settings to text box, specify the fully qualified name of the target archive. Type the path manually or click the Browse button browseButton.png and specify the target file in the dialog that opens .




To import settings from a JAR archive

On the main menu, choose File | Import Settings.
In the Import File Location dialog box that opens select the desired archive.
In the Select Components to Import dialog box that opens specify the settings to be imported, and click OK. By default, all settings are selected.