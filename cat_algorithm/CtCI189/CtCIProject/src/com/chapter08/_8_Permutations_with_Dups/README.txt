Crack the Coding Interview 189
Personal CCI 189 Answers

8.8 Permutations with Dups:
Write a method to compute all permutations of a string whose characters are not necessarily unique.
The list of permutations should not have duplicates.
