Crack the Coding Interview 189
Personal CCI 189 Answers

8.13 Stack of Boxes:
You have a stack of n boxes, with widths w(i), heights h(i), and depths d(i).
The boxes cannot be rotated and can only be stacked on top of one another if each box in the stack is strictly larger than the box above it in width, height, and depth.
Implement a method to compute the height of the tallest possible stack.
The height of a stack is the sum of the heights of each box.
