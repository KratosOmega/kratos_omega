package com.chapter02._1_Remove_Dups;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/22/16.
 * <p/>
 * Suppose to not using Temporary Buffer to solve this problem
 * Temporary Buffer = no creating extra space in the memory
 * -> Creating new reference to refer to the same location (list, in this case)
 * -> does not count as using Temporary Buffer.
 */
public class Solution {
    public void removeDuplicatesWithoutTempporaryBuffer(LinkedListNode list) {
        LinkedListNode next = list;
        LinkedListNode previous = list;
        LinkedListNode cursor = list;

        while (cursor != null) {
            next = cursor.getNext();
            previous = cursor;
            while (next != null){
                if (cursor.getData() == next.getData()){
                    next = next.getNext();
                    previous.setNext(next);
                }else {
                    next = next.getNext();
                    previous = previous.getNext();
                }
            }
            cursor = cursor.getNext();
        }
    }
}



//// ---------------- Old Version (v1) -------------------------
//public class Solution_A {
//    public void removeDuplicatesWithoutTempporaryBuffer(LinkedListNode list) {
//        LinkedListNode next = list;
//        LinkedListNode previous = list;
//        LinkedListNode cursor = list;
//
//        while (cursor.getNext() != null) {
//            next = cursor.getNext();
//            previous = cursor;
//            //list > 2
//            if (next.getNext() != null) {
//                while (next.getNext() != null) {
//                    if (cursor.getData() == next.getData()) {
//                        next = next.getNext();
//                        previous.setNext(next);
//                    } else {
//                        previous = next;
//                        next = next.getNext();
//                    }
//
//                    //after the above if/else, next.getNext() will be null when next reach the last node on the list
//                    //need the following if() statements to check if next.getNext() return null
//                    //this will be the last node to next
//                    if (next.getNext() == null) {
//                        if (cursor.getData() == next.getData()) {
//                            previous.setNext(null);
//                            break;
//                        }
//                    }
//                }
//            } else if (cursor.getData() == next.getData()) {
//                //list <= 2
//                cursor.setNext(null);
//                break; //This break is important
//                //next line will call for cursor.getNext()
//                //since cursor.setNext(null) will make its next to be null
//                //it will bug the while(cursor.getNext() != null)
//            }
//            cursor = cursor.getNext();
//        }
//    }
//}
