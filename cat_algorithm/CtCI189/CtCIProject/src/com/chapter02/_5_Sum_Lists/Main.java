package com.chapter02._5_Sum_Lists;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/23/16.
 */
public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();

        //******************************************** backwardSumList
        LinkedListNode b_numA = new LinkedListNode(7);
        b_numA.appendAtTail(1);
        b_numA.appendAtTail(6);

        LinkedListNode b_numB = new LinkedListNode(5);
        b_numB.appendAtTail(9);
        b_numB.appendAtTail(2);
        //******************************************** forwardSumList
        LinkedListNode f_numA = new LinkedListNode(6);
        f_numA.appendAtTail(1);
        f_numA.appendAtTail(7);

        LinkedListNode f_numB = new LinkedListNode(2);
        f_numB.appendAtTail(9);
        f_numB.appendAtTail(5);

        //****************************** Testing of 'Solution_A - backward'
        System.out.println("Backward:");
        System.out.printf("(");
        b_numA.print();
        System.out.printf(") + (");
        b_numB.print();
        System.out.println(")");
        System.out.println("Result: ");
        solution.backwardSumList(b_numA, b_numB).print();
        System.out.println("");

        System.out.println("****************************");

        //****************************** Testing of 'Solution_A - forward'
        System.out.println("Forward:");
        System.out.printf("(");
        f_numA.print();
        System.out.printf(") + (");
        f_numB.print();
        System.out.println(")");
        System.out.println("Result: ");

        solution.forwardSumList(f_numA,f_numB).print();
        System.out.println("");
    }
}
