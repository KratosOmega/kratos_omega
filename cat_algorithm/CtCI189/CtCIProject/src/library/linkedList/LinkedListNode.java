package library.linkedList;

public class LinkedListNode {
    private int data;
    private LinkedListNode next;

    public LinkedListNode() {
        this.data = 0;
        this.next = null;
    }

    public LinkedListNode(final int data) {
        this.data = data;
        this.next = null;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public LinkedListNode getNext() {
        return next;
    }

    public void setNext(LinkedListNode next) {
        this.next = next;
    }

    public void appendAtTail(final int data) {
        LinkedListNode newNode = new LinkedListNode(data);
        LinkedListNode current = this;
        while (current.next != null) {
            current = current.next;
        }
        current.next = newNode;
    }

    //Utility static method
    public void print() {
        LinkedListNode node = this;
        while (node.getNext() != null) {
            System.out.printf(node.getData() + " -> ");
            node = node.getNext();
            if (node.getNext() == null) {
                System.out.printf(node.getData() + "");
            }
        }
    }

    public int size() {
        LinkedListNode node = this;
        int length = 0;
        while (node.getNext() != null) {
            length++;
            node = node.getNext();
            if (node.getNext() == null) {
                length++;
            }
        }
        return length;
    }
}
