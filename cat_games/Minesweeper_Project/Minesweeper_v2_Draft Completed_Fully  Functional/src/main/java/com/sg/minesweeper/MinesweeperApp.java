/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.minesweeper;

import com.sg.minesweeper.dto.Board;
import java.util.Scanner;

/**
 *
 * @author Kratos
 */
public class MinesweeperApp {

    public static void main(String[] args) {
        Board board = new Board(10, 10, 10);
        boolean quit = false;
        String coordinate = "";
        Scanner input = new Scanner(System.in);

        while (!quit && !board.isAllSweeped()) {
            board.refreshBoard();

            System.out.print("Please enter coordinate: ");
            coordinate = input.nextLine();
            quit = board.onClick(coordinate);
        }

    }
}
