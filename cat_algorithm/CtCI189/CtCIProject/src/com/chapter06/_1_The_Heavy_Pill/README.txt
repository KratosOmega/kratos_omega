Crack the Coding Interview 189
Personal CCI 189 Answers

6.1 The Heavy Pill:
You have 20 bootles of pills. 19 bottles have 1.0 gram pills, but one has pills of weight 1.1 grams.
Given a scale that provides an exact measurement, how would you find the heavy bottle?
You can only use the scale once.
