package com.chapter02._3_Delete_Middle_Node;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/23/16.
 */
public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        LinkedListNode list = new LinkedListNode();
        LinkedListNode originalList = new LinkedListNode();

        list.appendAtTail(1);
        list.appendAtTail(2);
        list.appendAtTail(3);
        list.appendAtTail(4);
        list.appendAtTail(5);
        list.appendAtTail(6);

        //Prepare the node which always start @ beginning
        originalList = list;
        //Navigate to the the target node @ 4
        //Prepare the node in order to ONLY hava access to that node 'list' @ 4
        list = list.getNext().getNext().getNext().getNext();
        System.out.println("Node is @ " + list.getData());

        System.out.println("Before deletion: ");
        originalList.print();
        System.out.println("");
        System.out.println("After deletion ");
        solution.deleteMiddleNode(list);
        originalList.print();
    }
}
