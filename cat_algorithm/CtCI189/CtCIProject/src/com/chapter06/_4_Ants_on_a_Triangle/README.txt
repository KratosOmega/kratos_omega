Crack the Coding Interview 189
Personal CCI 189 Answers

6.4 Ants on a Trangle:
There are three ants on different vertices of a triangle.
What is the probability of collision (between any two or all of them) if they start walking on the sides of the triangle?
Assume that each ant randomly picks a direction, with eigher direction being equally likely to be chosen, and that they walk at the same speed.
Similarly, find the probability of collision with "n" ants on an n-vertex polygon.
