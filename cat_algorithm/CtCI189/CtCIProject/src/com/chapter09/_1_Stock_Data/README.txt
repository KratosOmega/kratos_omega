Crack the Coding Interview 189
Personal CCI 189 Answers

9.1 Stock Data:
Imagine you are building some sort of service that will be called by up to 1,000 client applications to get simple end-of-day stock price information (open, close, high, low).
You may assume that you already have the dat, and you can store it in any format you wish.
How would you design the client-facing service that provides the information to client applications? You are responsile for the development, rollout, and ongoing monitoring and maintenance of the feed.
Describe the different methods you considered and why you would recommend your approach.
Your service can use any technologies you wish, and can distribute the information to the client applications in any mechanism you choose.
