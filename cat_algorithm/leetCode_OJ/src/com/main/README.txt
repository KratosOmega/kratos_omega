LeetCode-OJ
Personal LeetCode OJ Answers



This repository contains my personal answers of LeetCode OJ while I am preparing for Developer position. I use this as personal checklist and keep track all the problems that I had done. Any suggestions to the code optimization are welcome!



Personal Coding History
2 introduction class about C++ during Sophomore year
1 introduction class about Visual Basic during Junior Year
1 introduction class about SQL during Senior Year
Information Systems & Accounting, BS Degrees of University of Maryland, College Park
Recently, started Java Self-Learning on February, 2016



Problem List
=> (3) Longest Substring Without Repeating Characters:
   --> https://leetcode.com/problems/longest-substring-without-repeating-characters/
   --> Solved on: 03/07/2016
   --> Time costed: about 6 hours

=> (36) Valid Sudoku :
   --> https://leetcode.com/problems/valid-sudoku/
   --> Solved on: 03/9/2016
   --> Time costed: about 10 hours

=> (43) Trapping Rain Water:
   --> https://leetcode.com/problems/trapping-rain-water/
   --> Solved on: 03/10/2016
   --> Time costed: about 4 hours

=> (151) Reverse Words in a String: 
   --> https://leetcode.com/problems/reverse-words-in-a-string/
   --> Solved on: 03/10/2016
   --> Time costed: about 1 hour
