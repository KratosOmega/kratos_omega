package com.chapter01._5_One_Away;

/**
 * Created by Kratos on 4/20/16.
 */

public class Solution {
    public boolean oneOrNoEdit(String s1, String s2){
        boolean replace = false;
        char[] c1 = s1.toCharArray();
        char[] c2 = s2.toCharArray();
        int diff = c1.length - c2.length;

        if (diff > 1 || diff < -1){
            return false;
        }

        if (diff == 0){
            for (int i = 0; i < c1.length; i++){
                if (c1[i] - c2[i] == 0){
                    continue;
                }else{
                    if(replace == false){
                        replace = true;
                    }else{
                        return false;
                    }
                }
            }
            return true;
        }else{
            if (c1.length > c2.length){
                try{
                    insertOrRemove(c1,c2);
                }catch(ArrayIndexOutOfBoundsException e){
                    return true;
                }
            }else{
                try{
                    insertOrRemove(c2,c1);
                }catch(ArrayIndexOutOfBoundsException e) {
                    return true;
                }
            }
        }
        return true;
    }

    private boolean insertOrRemove(char[] big, char[]small){
        boolean insOrRev = false;
        int indexFixer = 0;

        for(int i = 0; i < big.length; i++){
            if(big[i] == small[i+indexFixer]){
                continue;
            }else{
                if(insOrRev == false){
                    insOrRev = true;
                    indexFixer = -1;
                }else{
                    return false;
                }
            }
        }
        return true;
    }
}
