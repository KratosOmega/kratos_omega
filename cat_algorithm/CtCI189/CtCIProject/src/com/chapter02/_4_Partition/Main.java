package com.chapter02._4_Partition;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/23/16.
 */
public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        LinkedListNode node = new LinkedListNode(3);

        node.appendAtTail(5);
        node.appendAtTail(8);
        node.appendAtTail(5);
        node.appendAtTail(10);
        node.appendAtTail(2);
        node.appendAtTail(1);

        System.out.println("Before partition: ");
        node.print();
        System.out.println("");
        System.out.println("After partition of 5:");
        solution.nodePartition(node, 5);
        node.print();

        System.out.println("");
        System.out.println("After partition of 1:");
        solution.nodePartition(node, 1);
        node.print();

        System.out.println("");
        System.out.println("After partition of 10:");
        solution.nodePartition(node, 10);
        node.print();

        System.out.println("");
        System.out.println("After partition of 0:");
        solution.nodePartition(node, 0);
        node.print();

        System.out.println("");
        System.out.println("After partition of 100:");
        solution.nodePartition(node, 100);
        node.print();

        System.out.println("");
        System.out.println("After partition of 6:");
        solution.nodePartition(node, 6);
        node.print();
    }
}
