package com.chapter01._7_Rotate_Matrix;

/**
 * Created by Kratos on 4/20/16.
 */

public class Solution {
    //clockWiseRotation:
    //Original matrix is read by regular order: m x n
    //Rotated matrix's row depends on inner loop [n]
    //Rotated matrix's column depends on updated index
    //updated index depends on outter loop [n]
    public int[][] clockWiseRotation(int[][] matrix) {
        int length = matrix.length;
        int indexRange = length - 1;
        int updatedIndex = 0;
        int[][] rotatedMatrix = new int[length][length];

        for (int m = 0; m < length; m++) {
            updatedIndex = indexRange - m;
            for (int n = 0; n < length; n++) {
                rotatedMatrix[n][updatedIndex] = matrix[m][n];
            }
        }
        return rotatedMatrix;
    }

    //counterClockWiseRotation:
    //Original matrix is read by regular order: m x n
    //Rotated matrix's column depends on outter loop [m]
    //Rotated matrix's row depends on updated index
    //updated index depends on inter loop [n]
    public int[][] counterClockWiseRotation(int[][] matrix){
        int length = matrix.length;
        int indexRange = length - 1;
        int updatedIndex = 0;
        int[][] rotatedMatrix = new int[length][length];

        for (int m = 0; m < length; m++) {
            for (int n = 0; n < length; n++) {
                updatedIndex = indexRange - n;
                rotatedMatrix[updatedIndex][m] = matrix[m][n];
            }
        }
        return rotatedMatrix;
    }
}
