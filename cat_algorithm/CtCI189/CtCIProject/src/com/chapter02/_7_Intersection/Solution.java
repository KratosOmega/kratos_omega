package com.chapter02._7_Intersection;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/24/16.
 */
public class Solution {
    public LinkedListNode intersection(LinkedListNode a, LinkedListNode b) {
        int aLength = a.size();
        int bLength = b.size();
        int count = 0;

        //move the node with longer length
        //so both nodes will start @ the same length to the end
        if (aLength > bLength) {
            int moveForward = aLength - bLength;
            while (count < moveForward) {
                a = a.getNext();
                count++;
            }
        } else {
            int moveForward = bLength - aLength;
            while (count < moveForward) {
                b = b.getNext();
                count++;
            }
        }

        //If common length = 1
        if (a.getNext() == null) {
            if (a == b) {
                return a;
            } else {
                return null;
            }
        }

        //If common length > 1
        while (a != null) {
            if (a == b) {
                return a;
            } else {
                a = a.getNext();
                b = b.getNext();
            }
        }
        return null;
    }
}


////---------------- Old Method (v1) ----------------------
//public class Solution_A {
//    public LinkedListNode intersection(LinkedListNode a, LinkedListNode b) {
//        int aLength = a.size();
//        int bLength = b.size();
//        int count = 0;
//
//        //move the node with longer length
//        //so both nodes will start @ the same length to the end
//        if (aLength > bLength) {
//            int moveForward = aLength - bLength;
//            while (count < moveForward) {
//                a = a.getNext();
//                count++;
//            }
//        } else {
//            int moveForward = bLength - aLength;
//            while (count < moveForward) {
//                b = b.getNext();
//                count++;
//            }
//        }
//
//        //If common length = 1
//        if (a.getNext() == null) {
//            if (a == b) {
//                return a;
//            } else {
//                return null;
//            }
//        }
//
//        //If common length > 1
//        while (a.getNext() != null) {
//            if (a == b) {
//                return a;
//            } else {
//                a = a.getNext();
//                b = b.getNext();
//            }
//            if (a.getNext() == null) {
//                if (a == b) {
//                    return a;
//                } else {
//                    return null;
//                }
//            }
//        }
//        return null;
//    }
//}
