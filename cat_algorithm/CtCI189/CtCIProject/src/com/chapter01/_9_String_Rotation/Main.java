package com.chapter01._9_String_Rotation;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.isRotation("water bottle", "erbottlewat");    //True
        solution.isRotation("water bottle", "erbolttewat");    //False
        solution.isRotation("asdfghj", "fghjasd");             //True
        solution.isRotation("water bottle", "rberbbsrtbr");    //False
        solution.isRotation("water bottle", "");               //False
    }
}
