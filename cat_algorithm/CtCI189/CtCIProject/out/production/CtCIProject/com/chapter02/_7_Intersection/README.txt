Crack the Coding Interview 189
Personal CCI 189 Answers

2.7 Intersection:
Given two (singly) linked lists, determine if the two lists intersect. Return the intersecting node. Note that the intersection is defined based on reference, not value. That is, if the kith linked list, then they are intersecting.