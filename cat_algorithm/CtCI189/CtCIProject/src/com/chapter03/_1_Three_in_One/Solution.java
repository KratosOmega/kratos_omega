package com.chapter03._1_Three_in_One;

import java.util.Arrays;

/**
 * Created by Kratos on 4/26/16.
 * <p/>
 * Reference: https://github.com/wzhishen/cracking-the-coding-interview/blob/master/src/chap03/Q1.java
 *
 * Also, there are several approaches to this problem
 *
 * 1)   Without using linked list concept
 *      Since no linked list concept is used here, we can not trace how each block of array is distributed
 *      Thus, we need and have to allocate the array evenly in a organizable way to work (unless we are told the capacities for each stack)
 *      There are 2 way (no limited to these 2 ways) to do it
 *
 * - a) Reference: https://shashank7s.blogspot.com
 *      Divide the single array into 3 parts:
 *      Stack A: use [0, n/3)
 *      Stack B: use [n/3, 2n/3)
 *      Stack C: use [2n/3, n)
 *
 *       0 1 2 3 4 5 6 7 8
 *      | | | | | | | | | |
 *      |  A  |  B  |  C  |
 *      Stack A: start @ 0
 *      Stack B: start @ 3
 *      Stack C: start @ 6
 *
 *      note: "[" means inclusive, and "(" means exclusive of the end point.
 *
 *      use <code>int[] stackPointer = {-1, -1, -1};</code> to manage the stacks' tops
 *      which actually looks like this: stackPointer = {0, n/3, 2n/3};
 *
 *      Then implement the 3 stack with this single array just like doing so for 1 stack
 *
 * - b) Using constant <code>POSITION_UPDATE</code>
 *      use <code>int[] stackPointer = {0, 1, 2}</code> to start the stacks' tops
 *      then continuously add or subtract <code>POSITION_UPDATE</code> from the <code>stackPointer</code> to update
 *
 *      | A1 | B1 | C1 | A2 | B2 | C2 | A3 | B3 | C3 |
 *      |--> A1 + 3 = A2 -->|
 *      |<-- A2 - 3 = A1 <--|
 *
 *           |--> B1 + 3 = B2 -->|
 *           |<-- B2 - 3 = B1 <--|
 *
 *                |--> C1 + 3 = C2 -->|
 *                |<-- C2 - 3 = C1 <--|
 *
 * 2)   Using linked list concept
 *      By using the linked list concept, we can locate the previous block for the same stack in a single array
 *      Thus, it makes dramatically allocation of stack spaces to different stack possible
 *
 */

public class Solution {
    private final int STACK_SIZE;
    private final int STACK_NUM;
    private int[] stackPointers;
    private int[] buffer;

    public Solution(int STACK_SIZE, int STACK_NUM) {
        this.STACK_SIZE = STACK_SIZE;
        this.STACK_NUM = STACK_NUM;
        this.stackPointers = new int[STACK_NUM];
        Arrays.fill(this.stackPointers, -1);
        buffer = new int[STACK_SIZE * STACK_NUM];
    }

    public void push(int stackNum, int item) {
        if (isFull(stackNum)) {
            throw new IllegalArgumentException("Stack " + stackNum + " is full!");
        }
        ++stackPointers[stackNum];  //stackPointers[stackNum]++;
        setTop(stackNum, item);
    }

    public int pop(int stackNum) {
        if (isEmpty(stackNum)) {
            throw new IllegalArgumentException("Stack " + stackNum + " is empty!");
        }
        --stackPointers[stackNum];  //stackPointers[stackNum]--;

        return getTop(stackNum);
    }

    public int peek(int stackNum) {
        if (isEmpty(stackNum)) {
            throw new IllegalArgumentException("Stack " + stackNum + " is empty!");
        }

        return getTop(stackNum);
    }

    public boolean isFull(int stackNum) {
        if (stackNum < 0 || stackNum >= STACK_NUM) {
            throw new IllegalArgumentException("Stack " + stackNum + " doen't exist!");
        }
        return stackPointers[stackNum] >= STACK_SIZE - 1;
    }

    public boolean isEmpty(int stackNum) {
        if (stackNum < 0 || stackNum >= STACK_NUM) {
            throw new IllegalArgumentException("Stack " + stackNum + " doen't exist!");
        }
        return stackPointers[stackNum] <= -1;
    }

    public void printStack(int stackNum) {
        if (stackNum < 0 || stackNum >= STACK_NUM) {
            throw new IllegalArgumentException("Stack " + stackNum + " doen't exist!");
        }
        int top = getBufferIndex(stackNum);
        int btm = stackNum * STACK_SIZE;
        System.out.printf("Stack " + stackNum + ": ");
        for (int i = btm; i <= top; ++i) {
            System.out.printf(buffer[i] + " ");
        }
        System.out.println("[TOP]");
    }

    public void printStacks() {
        for (int i = 0; i < STACK_NUM; ++i) {
            printStack(i);
        }
    }

    private int getTop(int stackNum) {
        return buffer[getBufferIndex(stackNum)];
    }

    private void setTop(int stackNum, int item) {
        buffer[getBufferIndex(stackNum)] = item;
    }

    private int getBufferIndex(int stackNum) {
        return stackPointers[stackNum] + stackNum * STACK_SIZE;
    }
}
