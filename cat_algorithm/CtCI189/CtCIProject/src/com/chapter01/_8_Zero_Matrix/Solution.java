package com.chapter01._8_Zero_Matrix;

/**
 * Created by Kratos on 4/20/16.
 */

public class Solution {
    public void zeroMatrix(int[][] matrix) {
        //Using boolean matrix
        boolean[] rows = new boolean[matrix.length];
        boolean[] cols = new boolean[matrix[0].length];

        // mark zero
        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[0].length; ++j) {
                if (matrix[i][j] == 0) {
                    rows[i] = true;
                    cols[j] = true;
                }
            }
        }

        // set zeros
        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[0].length; ++j) {
                if (rows[i] || cols[j]) {
                    matrix[i][j] = 0;
                }
            }
        }
        printMatrix(matrix);
    }

    private void printMatrix(int[][] matrix){
        for(int m = 0; m < matrix.length; m ++){
            for(int n = 0; n < matrix[0].length; n++){
                System.out.printf(matrix[m][n]+" ");
            }
            System.out.println("");
        }
    }
}
