package com.chapter02._2_Return_Kth_to_Last;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/23/16.
 */
public class Solution {
    //Recursive Approach:
    //1. Recurse through the Linked list
    //2. When we reach to the end of the list, base case will return 0
    //3. Now with each passing back call through stack, add 1 and return.
    //4. When count hits k, print the data.
    public int kthToEndByRecursion(LinkedListNode list, int k) {
        int sum = 0;
        LinkedListNode pointer = list;

        if (list == null) {
            return 0;
        }

        int i = kthToEndByRecursion(list.getNext(), k) + 1;
        if (i == k) {
            System.out.println(list.getData());
        }
        return i;
    }

    //Iterative Approach:
    //1. Take two pointers approach
    //2. Move first pointer by k
    //3. now move both the pointers and when the first pointer reaches the end of the list the second pointer will be at the kth node from the end.
    //4. Return the kth node data.
    public int kthToEndByIteration(LinkedListNode list, int k) {
        int counter = 1;
        LinkedListNode navigCursor = list;
        LinkedListNode destiCursor = list;

        //navigCursor runs the first part of the journey (B->k')
        //(B-----k'-----k-----E)
        //(B->k')+(k'->E) = (B->k)+(k->E) && (B->k') = (k->E)
        //Thus, (k'->E) = (B->k)
        while (counter < k) {
            navigCursor = navigCursor.getNext();
            counter++;
        }

        //navigCursor runs the second (last) part of the journey (k'->E)
        //since (k'->E) = (B->k)
        //destiCursor run for (B->k) will be the answer
        while (navigCursor.getNext() != null) {
            navigCursor = navigCursor.getNext();
            destiCursor = destiCursor.getNext();
        }
        return destiCursor.getData();
    }

    public int kthToEndByLength(LinkedListNode list, int k) {
        int len = list.size();
        int kthToBeginning = len - k + 1;
        int counter = 1;

        //when list.size() == 1
        if (list.getNext() == null) {
            return list.getData();
        }

        //when list.size() > 1
        while (list != null) {
            if (counter < kthToBeginning) {
                list = list.getNext();
                counter++;
            } else {
                return list.getData();
            }
        }
        return list.getData();
    }
}
