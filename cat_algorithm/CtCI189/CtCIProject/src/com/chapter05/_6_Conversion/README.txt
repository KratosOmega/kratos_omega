Crack the Coding Interview 189
Personal CCI 189 Answers

5.6 Conversion:
Write a function to determine the number of bits you would need to flip to convert integer A to integer B.

EXAMPLE
Input:      29  (or: 11101),  15  (or: 01111)
Output:     2


