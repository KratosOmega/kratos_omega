Crack the Coding Interview 189
Personal CCI 189 Answers

9.6 Sales Rank:
A large eCommerce company wishes to list the best-selling products, overall and by category.
For example, one product might be the #1056th best-selling product overall,
but the #13th best-selling product under "Sport Equipment",
and the #24th best-selling product under "Safety."

Describe how you would design this system.
