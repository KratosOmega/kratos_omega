package com.chapter01._7_Rotate_Matrix;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();

        int[][] matrix = {{1,2,3},{4,5,6},{7,8,9}};

        System.out.println(">>>>>>>>>> Original Matrix <<<<<<<<<<");
        for(int m = 0; m< matrix.length; m++){
            for (int n = 0; n < matrix.length; n++){
                System.out.printf(matrix[m][n] + " ");
            }
            System.out.println("");
        }

        System.out.println(">>>>>>>>>> Clockwise Rotation <<<<<<<<<<");

        int [][] rotatedMatrix = solution.clockWiseRotation(matrix);
        for(int m = 0; m< rotatedMatrix.length; m++){
            for (int n = 0; n < rotatedMatrix.length; n++){
                System.out.printf(rotatedMatrix[m][n] + " ");
            }
            System.out.println("");
        }

        System.out.println(">>>>>>>>>> Counter-Clockwise Rotation <<<<<<<<<<");

        int [][] counterRotatedMatrix = solution.counterClockWiseRotation(matrix);
        for(int m = 0; m< counterRotatedMatrix.length; m++){
            for (int n = 0; n < counterRotatedMatrix.length; n++){
                System.out.printf(counterRotatedMatrix[m][n] + " ");
            }
            System.out.println("");
        }
    }
}
