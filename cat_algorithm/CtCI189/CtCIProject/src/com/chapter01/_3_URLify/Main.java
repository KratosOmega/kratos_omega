package com.chapter01._3_URLify;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.urlify("    hello world my name is     Kratos    "));
    }
}
