package com.chapter01._4_Palindrome_Permutation;

/**
 * Created by Kratos on 4/19/16.
 */

public class Solution {
    public boolean isPermutationAndPalindrome(String str){
        String string = removeWhiteSpace(str);
        char[] stringList = string.toCharArray();
        int[] charList = new int[128];
        boolean odd = false;

        for (int c : stringList){
            charList[c]++;
        }

        for (int i : charList){
            if(i%2 == 0 ){
                continue;
            }else{
                if(odd == false){
                    odd = true;
                }else{
                    System.out.println("False");
                    return false;
                }
            }
        }
        System.out.println("True");
        return true;
    }

    //Private method to remove white spaces of the input string
    private String removeWhiteSpace(String str){
        char[] stringList = str.toCharArray();
        String newString = "";

        for(char c : stringList){
            if (c != ' '){
                newString += c;
            }
        }
        return newString;
    }
}
