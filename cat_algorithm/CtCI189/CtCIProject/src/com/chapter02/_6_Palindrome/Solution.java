package com.chapter02._6_Palindrome;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/24/16.
 * <p/>
 * This problem can be solved with 2 methods
 * <p/>
 * 1) Stack method (recursive method)
 * A simple solution is to use a stack of list nodes.
 * Time complexity of this method is O(n), but it requires O(n) extra space.
 * This mainly involves three steps.
 * - Traverse the given list from head to tail and push every visited node to stack.
 * - Traverse the list again. For every visited node, pop a node from stack and compare data of popped node with currently visited node.
 * - If all nodes matched, then return true, else false.
 * <p/>
 * <p/>
 * 2) Reverse method
 * This method takes O(n) time and O(1) extra space.
 * - Get the middle of the linked list.
 * - Reverse the second half of the linked list.
 * - Check if the first half and second half are identical.
 * - Construct the original linked list by reversing the second half again and attaching it back to the first half
 * <p/>
 * My solution is using the 2) Reverse method
 * <p/>
 * I may come back to update the 1) Stack method
 */
public class Solution {
    public boolean isPalindrome_Reverse(LinkedListNode node) {
        LinkedListNode frontHalf = new LinkedListNode();
        LinkedListNode backHalf = new LinkedListNode();
        int length = node.size();
        int even;
        int odd;

        //---------------- When node = 1
        //If only 1 node, does it count as Palindrome???
        if (length == 1) {
            return true;
        }

        //---------------- When node = 2
        if (length == 2) {
            if (node.getData() == node.getNext().getData()) {
                return true;
            } else {
                return false;
            }
        }

        //---------------- When node > 2
        //clone the front half & back half
        //When middle node does not exist
        if (length % 2 == 0) {
            //clone the front half
            even = 1;
            LinkedListNode updateFrontHalf = frontHalf;
            while (even < length / 2) {
                updateFrontHalf.setData(node.getData());
                node = node.getNext();
                updateFrontHalf.setNext(new LinkedListNode(node.getData()));
                updateFrontHalf = updateFrontHalf.getNext();
                even++;
            }
            //clone the back half
            even = 1;
            node = node.getNext();
            LinkedListNode updateBackHalf = backHalf;
            while (even < length / 2) {
                if (updateBackHalf.getNext() == null) {
                    updateBackHalf.setNext(new LinkedListNode(node.getData()));
                    node = node.getNext();
                    updateBackHalf.setData(node.getData());
                    even++;
                } else {
                    LinkedListNode tempNode = new LinkedListNode(node.getData());
                    tempNode.setNext(updateBackHalf.getNext());
                    updateBackHalf.setNext(tempNode);
                    node = node.getNext();
                    updateBackHalf.setData(node.getData());
                    even++;
                }
            }
            //when the middle node does exist
        } else {
            //clone the front half
            odd = 1;
            LinkedListNode updateFrontHalf = frontHalf;
            while (odd < (length - 1) / 2) {
                updateFrontHalf.setData(node.getData());
                node = node.getNext();
                updateFrontHalf.setNext(new LinkedListNode(node.getData()));
                updateFrontHalf = updateFrontHalf.getNext();
                odd++;
            }
            //clone the back half
            odd = 1;
            node = node.getNext().getNext();
            LinkedListNode updateBackHalf = backHalf;
            while (odd < (length - 1) / 2) {
                if (updateBackHalf.getNext() == null) {
                    updateBackHalf.setNext(new LinkedListNode(node.getData()));
                    node = node.getNext();
                    updateBackHalf.setData(node.getData());
                    odd++;
                } else {
                    LinkedListNode tempNode = new LinkedListNode(node.getData());
                    tempNode.setNext(updateBackHalf.getNext());
                    updateBackHalf.setNext(tempNode);
                    node = node.getNext();
                    updateBackHalf.setData(node.getData());
                    odd++;
                }
            }
        }

        //Compare frontHalf & backHalf linked list
        while (frontHalf != null) {
            if (frontHalf.getData() != backHalf.getData()) {
                return false;
            } else {
                frontHalf = frontHalf.getNext();
                backHalf = backHalf.getNext();
            }
        }
        return true;
    }
}