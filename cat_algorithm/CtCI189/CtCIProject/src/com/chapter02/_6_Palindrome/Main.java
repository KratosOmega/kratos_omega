package com.chapter02._6_Palindrome;

import library.linkedList.LinkedListNode;

/**
 * Created by Kratos on 4/24/16.
 */
public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();

        LinkedListNode node = new LinkedListNode (0);
        node.appendAtTail(2);
//        node.appendAtTail(2);
//        node.appendAtTail(2);
//        node.appendAtTail(2);
//        node.appendAtTail(1);
//        node.appendAtTail(0);
//        node.appendAtTail(1);
//        node.appendAtTail(2);
//        node.appendAtTail(2);
//        node.appendAtTail(2);
        node.appendAtTail(2);
        node.appendAtTail(0);

        if (solution.isPalindrome_Reverse(node)){
            node.print();
            System.out.println("");
            System.out.println("True");
        }else {
            node.print();
            System.out.println("");
            System.out.println("False");
        }


    }
}
