Crack the Coding Interview 189
Personal CCI 189 Answers

8.5 Recursive Multiply:
Write a recursive function to multiply two positive integers without using the "*" operator.
You can use "addition", "substraction", "bit shifting", but you should minimize the number of those operations.

