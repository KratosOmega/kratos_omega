Crack the Coding Interview 189
Personal CCI 189 Answers

6.9 100 Lockers:
There are 100 closed lockers in a hallway. A man begins by opening all 100 lockers.
Next, he closes every second locker. Then, on his third pass, he toggles every third locker (closes it if it is open or opens it if it is closed).
This process continues for 100 pass in the hallway, in which he toggles only locker #100, how many lockers are open?
